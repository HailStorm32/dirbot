//  dirbot, a bot to simplify the listing of matrix directories
//  Copyright 2019 Waylon Cude
//
//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.

use serde::Deserialize;

const PAGE_SIZE: usize = 20;

#[derive(Deserialize)]
pub struct Rooms {
    chunk: Vec<Room>
}

impl Rooms {
    // Show 5 rooms at a time so we don't spam any channel
    pub fn get_page(&self, page: usize) -> impl Iterator<Item=&Room> {
        if page > 0 { 
            self.get_active_rooms().skip((page-1)*PAGE_SIZE).take(PAGE_SIZE)        
        } else {
            self.get_active_rooms().skip(0).take(PAGE_SIZE)        
        }
    }
    // Only show rooms with aliases
    fn get_active_rooms(&self) -> impl Iterator<Item=&Room> {
        self.chunk.iter().filter(|x| x.aliases.is_some() && x.aliases.as_ref().unwrap().len() > 0 )
    }
}

#[allow(unused)]
#[derive(Deserialize)]
pub struct Room {
    aliases: Option<Vec<String>>,
    canonical_alias: Option<String>,
    guest_can_join: bool,
    name: String,
    num_joined_members: isize,
    room_id: String,
    topic: Option<String>,
    world_readable: bool
}

impl Room {
    pub fn to_line(&self) -> String {
        match self.topic {
            Some(ref topic) => format!("#{:20} {:35} ({}): {}",self.name,self.canonical_alias.as_ref().unwrap(),self.num_joined_members,topic),
            None => format!("#{:20} {:35} ({})",self.name,self.canonical_alias.as_ref().unwrap(),self.num_joined_members),
        }
    }
}
