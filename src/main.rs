//  dirbot, a bot to simplify the listing of matrix directories
//  Copyright 2019 Waylon Cude
//
//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.

mod chunks;

use chunks::Rooms;

use std::fmt::Write;
use std::str;

use matrix_bot_api::handlers::{HandleResult, StatelessHandler};
use matrix_bot_api::MessageType;
use matrix_bot_api::{MatrixBot};

// YEET
static mut HOMESERVER_URL: String = String::new();

fn main() {
    println!("Starting infobot");
    let mut settings = config::Config::default();
    settings.merge(config::File::with_name("config.toml")).unwrap();
    println!("Read in config file");

    // Read configuration from config.toml
    let user = settings.get_str("user").unwrap();
    let password  = settings.get_str("password").unwrap();
    let homeserver_url = settings.get_str("homeserver_url").unwrap();

    unsafe {
        HOMESERVER_URL = homeserver_url.clone();
    }

    // Load the database
    let mut handler = StatelessHandler::new();
    handler.register_handle("directory", |bot, message, tail| {
        let homeserver_url = unsafe { &HOMESERVER_URL };
        match get_message(&homeserver_url,&tail) {
            Some(response) => bot.send_message(&response, &message.room, MessageType::TextMessage),
            None           => {}
        }
        HandleResult::StopHandling
    });
    handler.register_handle("dir", |bot, message, tail| {
        let homeserver_url = unsafe { &HOMESERVER_URL };
        match get_message(&homeserver_url,&tail) {
            Some(response) => bot.send_message(&response, &message.room, MessageType::TextMessage),
            None           => {}
        }
        HandleResult::StopHandling
    });

    // And set up the bot
    let bot = MatrixBot::new(handler);
    println!("Starting bot");
    bot.run(&user,&password,&homeserver_url);

}
fn get_message(homeserver_url: &str, tail: &str) -> Option<String> {
    let resp: Rooms = reqwest::get(&format!("{}/_matrix/client/r0/publicRooms",homeserver_url)).ok()?
        .json().ok()?;
    let tail = tail.trim();
    let rooms = if tail == "" {
        resp.get_page(1)
    } else {
        resp.get_page(str::parse(tail).ok()?)
    };
    let mut response = String::new();
    writeln!(&mut response,"<pre><code>").ok()?;
    for room in rooms {
        writeln!(&mut response,"{}",room.to_line()).ok()?;
    }
    writeln!(&mut response,"</code></pre>").ok()?;
    Some(response)
}
